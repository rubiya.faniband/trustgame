
public class Machine {

    private final int[] COCO = {2, 2};
    private final int[] COCH = {-1, 3};
    private final int[] CHCO = {3, -1};
    private final int[] CHCH = {0, 0};

    public int[] calculate(MoveType.Moves player1Move, MoveType.Moves player2Move) {
        int[] score = CHCH;
        if (player1Move.equals(MoveType.Moves.CHEAT) && player2Move.equals(MoveType.Moves.COOPERATE)) {
            score = CHCO;
        } else if (player1Move.equals(MoveType.Moves.COOPERATE) && player2Move.equals(MoveType.Moves.CHEAT)) {
            score = COCH;
        } else if (player1Move.equals(MoveType.Moves.COOPERATE) && player2Move.equals(MoveType.Moves.COOPERATE)) {
            score = COCO;
        }
        return (score);
        /* Test Comment */
    }
}
