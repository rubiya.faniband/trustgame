import java.util.ArrayList;

public class Player {
    ArrayList<Integer> scores ;
    PlayerBehaviour playerBehaviour;
    public Player(PlayerBehaviour playerBehaviour) {
        scores = new ArrayList<>();
        this.playerBehaviour = playerBehaviour;
    }

    public MoveType.Moves makeAMove() {
        return playerBehaviour.makeAMove();
    }

}
