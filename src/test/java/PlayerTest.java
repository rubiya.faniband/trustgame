import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import java.util.Scanner;

public class PlayerTest {
    private MoveType.Moves runEmulatedScanner(String data) {
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Scanner scanner = new Scanner(System.in);
        Player player = new Player(new ConsolePlayerBehaviour(scanner));
        return(player.makeAMove());
    }

    @Test
    public void moveToCheatOn0(){
        String data = "0";
        Assert.assertEquals(MoveType.Moves.CHEAT,runEmulatedScanner(data));
    }

    @Test
    public void moveToCooperateOn1(){
        String data = "1";
        Assert.assertEquals(MoveType.Moves.COOPERATE,runEmulatedScanner(data));
    }

    @Test
    public void moveToInvalidInput(){
        String data = "3";
        Assert.assertEquals(MoveType.Moves.INVALID,runEmulatedScanner(data));
    }

    @Test
    public void CooperatePlayerTest(){
        String data = "8";
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        Player player = new Player(new CooperatePlayerBehaviour());
        Assert.assertEquals(MoveType.Moves.COOPERATE, player.makeAMove());

    }
}
